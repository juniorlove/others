## Descriptions
This file try to calculate the corresponding parameters of a specific equation, for which there are two ways to obtain it:
1. Linear recession for all experimental data;
2. Calculate the equation with specific parameters of the data group;