clc; clear; close all;
fprintf('To fit the exponential recession equation.\nProcessing');
%% Parameters
% filename and filepath
rdir = 'C:\Users\Hsueh\Desktop';
rfile = fullfile(rdir, "P66614 AOT.txt");
% load file
rdata = readtable(rfile);
t = rdata.Var1;
eta = rdata.Var2;
% others
r2l = 100; % Calculation times of solution 2
fdata2 = zeros(r2l, 3); % format: t0, b, c
%% Solution 1: recession
frange = -10000:10000;
fdata = table('Size', [length(frange) 3], ...
    'VariableTypes', repmat("double", 1, 3), 'VariableNames', ["t0", "b", "c"]);
i = 1;

for t0 = frange
    fdata.t0(i) = t0;
    [fdata.c(i), fdata.b(i)] = yfit(eta, t, t0);
    i = i + 1;
end

figure;
plot3(fdata.t0, fdata.b, fdata.c);
xlabel('t0');
ylabel('b');
zlabel('c');

%% Solution 2: solve equation
for i = 1:r2l
    fdata2(i, :) = yslv(eta, t, randperm(height(rdata), 3));
end

fprintf('\nfi\n');

function [c, b] = yfit(eta, t, t0)
    x = 1 / (t - t0);
    y = log(eta);
    rc = polyfit(x, log(y), 1);
    c = rc(1);
    b = exp(rc(2));
end

function yans = yslv(eta, t, i)
    syms t0 b c
    eqns = [
            log(eta(i(1))) == log(b) - c / (t(i(1)) - t0), ...
                log(eta(i(2))) == log(b) - c / (t(i(2)) - t0), ...
                log(eta(i(3))) == log(b) - c / (t(i(3)) - t0)
            ];
    vars = [t0 b c];
    [t0, b, c] = solve(eqns, vars);
    yans = [t0, b, c];
end
